pki-openssl
===========

This shell script implements a subset of strongSwan's [`ipsec pki`][PKI]
commands using [OpenSSL][OPENSSL].

The purpose is to simplify setting up keys and certificates for
applications such as TLS web servers or VPNs. Its reason for existence is
the truly awful user interface of OpenSSL which requires handling multiple
(configuration) files for most basic actions. In contrast, this tool
accepts settings on the command line and works as a standard filter.

While the tool follows ipsec's `pki` commands in spirit, there are several
differences to simplify the implementation.

 1. Only very few options are supported.

 2. Only short forms of options are supported. Commands are simple names
    rather than options.

 3. Subject alt names given to the `-a` option have to be specified
    like this: `DNS:www.example.com`, `IP:1.2.3.4`,
    `email:client@vpn.server.com`.

 4. Distinguished names have to be specified in the form
    `/C=CH/O=Acme/CN=name`.

 5. The `issue` command requires a private key instead of a public key.


Usage
-----

    pki gen
    pki help
    pki issue [-a subjectAltName][-d subject-dn]
              [-e ikeIntermediate|serverAuth] -c file -k file
    pki print [-t priv|pub|x509]
    pki pub
    pki self [-b] -d distinguished-name

<dl>
 <dt>gen</dt>
 <dd>Generate a 2048 bit RSA key.</dd>

 <dt>help</dt>
 <dd>Show usage.</dd>

 <dt>issue</dt>
 <dd>
  Sign a private key with a CA's private key.

  <dl>
   <dt>-a <var>subjectAltName</var></dt>
   <dd>The subjectAltName to include in the certificate.</dd>

   <dt>-c <var>file</var></dt>
   <dd>CA certificate file.</dd>

   <dt>-d <var>subject-dn</var></dt>
   <dd>Subject distinguished name.</dd>

   <dt>-e ikeIntermediate|serverAuth</dt>
   <dd>Include extendedKeyUsage.</dd>

   <dt>-k <var>file</var></dt>
   <dd>CA private key file.</dd>
  </dl>
 </dd>

 <dt>print</dt>
 <dd>
  Print credentials in human-readable form.

  <dl>
   <dt>-t priv|pub|x509</dt>
   <dd>The type of credential, default: x509.</dd>
  </dl>
 </dd>

 <dt>pub</dt>
 <dd>Read a private key and output the public key.</dd>

 <dt>self</dt>
 <dd>
  Read a private key and output a self-signed certificate.

  <dl>
   <dt>-b</dt>
   <dd>Include CA basicConstraint.</dd>

   <dt>-d <var>distinguished-name</var></dt>
   <dd>Subject and issuer distinguished name.</dd>
  </dl>
 </dd>
</dl>


Examples
--------


Generate a private key and a self-signed certificate for a CA.

    pki gen | tee MyCA.key |
    pki self -bd '/C=CH/O=Example/CN=MyCA' >MyCA.crt

Generate a private key and a signed certificate for a TLS web server or VPN
responder. The subject alt name(s) should match what clients will use to
connect.

    pki gen | tee vpn.example.com.key |
    pki issue \
        -a DNS:vpn.example.com \
        -c MyCA.crt -k MyCA.key \
        -d '/C=CH/O=Example/CN=vpn.example.com' \
        -e ikeIntermediate -e serverAuth \
        >vpn.example.com.crt

Generate a private key and a signed certificate for a VPN initiator.

    pki gen | tee client@vpn.example.com.key |
    pki issue \
        -a email:client@vpn.example.com \
        -c MyCA.crt -k MyCA.key \
        -d '/C=CH/O=Example/CN=client' \
        >client@vpn.example.com.crt

Package the CA certificate and the client's private key and certificate as
PKCS#12 for ease of distribution.

    cat MyCA.crt client@vpn.example.com.crt |
    openssl pkcs12 \
        -export -inkey client@vpn.example.com.key \
        >client@vpn.example.com.p12


[PKI]: https://wiki.strongswan.org/projects/strongswan/wiki/IpsecPKI
[OPENSSL]: https://www.openssl.org/
